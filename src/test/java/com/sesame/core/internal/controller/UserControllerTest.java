package com.sesame.core.internal.controller;

import com.sesame.core.DemoApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * Created by will on 21/05/2017.
 */





@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DemoApplication.class)
@WebAppConfiguration
public class UserControllerTest {

    @Autowired private WebApplicationContext ctx;

    private MockMvc mockMvc;

    /*@Configuration
    public static class TestConfiguration {

        @Bean public LoginController loginController() {
            return new LoginController();
        }


        @Bean
        public UserRepository userRepository() {
            return Mockito.mock(UserRepository.class);
        }


    }*/

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
    }

    @Test
    public void getLogin() throws Exception {

    }

    @Test
    public void postLogin() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders
                .get("/login")
        .accept(MediaType.ALL))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void greeting() throws Exception {

    }

}