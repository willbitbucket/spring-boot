package com.sesame.core.internal.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * Created by will on 19/05/2017.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name="user")
public class User extends Base{
    @Id
    @GeneratedValue
    private String id;

    @NotNull
    @Size(min = 4)
    private String username;

    @NotNull
    @Size(min = 4)
    private String password;


    @Transient
    @Size(min = 4)
    private String confirmPassword;


    @Transient
    @Size(min = 4)
    private String newPassword;

   // private Date birth;


}
