package com.sesame.core.internal.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * Created by will on 21/05/2017.
 */
@MappedSuperclass
public class Base {
    @Version
    @Column(name="version")
    private Long entityVersion;
}
