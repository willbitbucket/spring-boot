package com.sesame.core.internal.controller;


import com.sesame.core.internal.entity.User;
import com.sesame.core.internal.exception.NotFoundException;
import com.sesame.core.internal.repository.UserRepository;
import com.sesame.core.internal.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

/**
 * Created by will on 19/05/2017.
 */
@Controller
public class UserController {

    private Logger LOG = Logger.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @RequestMapping(value = "/reg", method= RequestMethod.GET)
    public String getLogin(User user){
        return "reg";
    }

    @RequestMapping(value = "/reg", method= RequestMethod.POST)
    public String postLogin(@Valid User user, BindingResult bd, Model model){
        if(bd.hasErrors()) {
            System.out.println(bd.getAllErrors().toString());
            return "reg";
        }
        else
            if(user.getPassword().equals(user.getNewPassword()))
            {
                user = userService.save(user);
                model.addAttribute("user",user);
                return "result";
            }else {

                model.addAttribute("msg","password is not the same as confirm password");
                return "reg";
            }

    }

    @RequestMapping(value = "/reset", method= RequestMethod.GET)
    public String getReset( User user){
        return "reset";
    }

    @RequestMapping(value = "/reset", method= RequestMethod.POST)
    public String getReset(@Valid  User user, BindingResult bd, Model model){
        if(bd.hasErrors()) {
            System.out.println(bd.getAllErrors().toString());
            return "reset";
        }
        else {
            if(user.getPassword().equals(user.getNewPassword()))
            {

                LOG.info("password is the same as current password");
                model.addAttribute("msg","new password is the same as current password");
                return "reset";
            }

            User existingUser = userService.findByUsernameAndPassword(user.getUsername(), user.getPassword());
            if(existingUser!=null){
                existingUser.setPassword(user.getNewPassword());
                userService.save(existingUser);
                model.addAttribute("user",existingUser);
            }else{
                model.addAttribute("msg","incorrect username/passowrd");
                //throw new NotFoundException();
                return "reset";
            }
            return "result";
        }

    }

    @RequestMapping(value = "/greeting", method= RequestMethod.GET)
    public String greeting(){

        ModelAndView modelAndView = new ModelAndView();
        return "greeting";
    }
}
