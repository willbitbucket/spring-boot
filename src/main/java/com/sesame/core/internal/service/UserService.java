package com.sesame.core.internal.service;

import com.sesame.core.internal.entity.User;
import com.sesame.core.internal.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Will on 2017/5/21.
 */
@Service
public class UserService {
    @Autowired
    UserRepository userRepo;

    public User save(User user){
       return userRepo.save(user);
    }

    public User findByUsernameAndPassword(String username, String password)
    {
        return userRepo.findByUsernameAndPassword(username, password);
    }

}
