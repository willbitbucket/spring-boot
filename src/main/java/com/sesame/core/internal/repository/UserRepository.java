package com.sesame.core.internal.repository;

import com.sesame.core.internal.entity.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by will on 20/05/2017.
 */
@org.springframework.stereotype.Repository
public interface UserRepository extends CrudRepository<User, String>, UserRepositoryCustom {
    public User findByUsernameRegex(String reg);

    public User findByUsernameAndPassword(String username, String password);

}
