package com.sesame.core.internal.repository;

import com.sesame.core.internal.entity.User;

/**
 * Created by will on 20/05/2017.
 */
@org.springframework.stereotype.Repository
public interface UserRepositoryCustom {
    public User findByUsernameRegex(String reg);
}
